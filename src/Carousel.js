import React from 'react';

function Carousel(carouselProps) {
    return (
        <div className="carousel">
            <a href="#" className="carousel-item"><img src={carouselProps.img1} /></a>
            <a href="#" className="carousel-item"><img src={carouselProps.img2} /></a>
            <a href="#" className="carousel-item"><img src={carouselProps.img3} /></a>
            <a href="#" className="carousel-item"><img src={carouselProps.img4} /></a>
            <a href="#" className="carousel-item"><img src={carouselProps.img5} /></a>
        </div>
    );
}

export default Carousel;