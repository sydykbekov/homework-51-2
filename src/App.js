import React from 'react';
import './App.css';
import Header from './Header';
import NewLogo from './NewLogo';
import Carousel from './Carousel';
import Genres from './Genres';
import Footer from './Footer';


const App = (props) => {
    return (
        <div className="App">
            <Header logo="MUVIK" firstLi="Бестселлеры" secondLi="Сериалы" thirdLi="Мультфильмы" />
            <NewLogo />
            <Carousel img1="img/img1.jpg" img2="img/img2.jpg" img3="img/img3.jpg" img4="img/img4.jpg" img5="img/img5.jpg" />
            <Genres genre1="Фантастика" genre2="Боевики" genre3="Комедии" genre4="Мультфильмы" genre5="Драма/Мелодрама" genre6="Сериалы" genre7="Анимэ"/>
            <Footer link1="Чёрная пантера (2018)" link2="Мстители: Война бесконечности (2018)" link3="Тор: Рагнарок (2017)" link4="Дедпул 2 (2018)"/>
        </div>
    )
};

export default App;
