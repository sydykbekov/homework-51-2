import React from 'react';

function Genres(genreProps) {
    return (
        <div className="collection">
            <a href="#" className="collection-item">{genreProps.genre1}</a>
            <a href="#" className="collection-item">{genreProps.genre2}</a>
            <a href="#" className="collection-item">{genreProps.genre3}</a>
            <a href="#" className="collection-item">{genreProps.genre4}</a>
            <a href="#" className="collection-item">{genreProps.genre5}</a>
            <a href="#" className="collection-item">{genreProps.genre6}</a>
            <a href="#" className="collection-item">{genreProps.genre7}</a>
        </div>
    );
}

export default Genres;