import React from 'react';

function Header(headerProps) {
    return (
        <nav>
            <div className="nav-wrapper">
                <a href="#" className="brand-logo">{headerProps.logo}</a>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
                    <li><a href="#">{headerProps.firstLi}</a></li>
                    <li><a href="#">{headerProps.secondLi}</a></li>
                    <li><a href="#">{headerProps.thirdLi}</a></li>
                </ul>
            </div>
        </nav>
    );
}

export default Header;