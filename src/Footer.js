import React from 'react'

function Footer(footerProps) {
    return(
        <footer className="page-footer">
            <div className="container">
                <div className="row">
                    <div className="col l6 s12">
                        <h5 className="white-text">MUVIK</h5>
                        <p className="grey-text text-lighten-4">У нас вы найдете лучшие фильмы «Золотой эпохи» Голливуда, классическое европейское кино, лучшие французские комедии, азиатское кино, советские картины и современные хиты.</p>
                    </div>
                    <div className="col l4 offset-l2 s12">
                        <h5 className="white-text">Скоро на сайте:</h5>
                        <ul>
                            <li><a className="grey-text text-lighten-3" href="#">{footerProps.link1}</a></li>
                            <li><a className="grey-text text-lighten-3" href="#">{footerProps.link2}</a></li>
                            <li><a className="grey-text text-lighten-3" href="#">{footerProps.link3}</a></li>
                            <li><a className="grey-text text-lighten-3" href="#">{footerProps.link4}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="footer-copyright">
                <div className="container">
                    © 2014 Copyright Text
                    <a className="grey-text text-lighten-4 right" href="#">More Links</a>
                </div>
            </div>
        </footer>
    );
}

export default Footer;